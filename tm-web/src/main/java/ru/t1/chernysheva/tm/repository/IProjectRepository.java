package ru.t1.chernysheva.tm.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.t1.chernysheva.tm.model.ProjectDTO;

@Repository
public interface IProjectRepository extends JpaRepository<ProjectDTO, String> {
}
