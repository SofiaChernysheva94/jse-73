package ru.t1.chernysheva.tm.configuration;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@ComponentScan("ru.t1.chernysheva.tm")
public class ApplicationConfiguration implements WebMvcConfigurer {

}
