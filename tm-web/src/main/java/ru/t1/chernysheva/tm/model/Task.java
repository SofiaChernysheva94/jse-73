package ru.t1.chernysheva.tm.model;

import ru.t1.chernysheva.tm.enumerated.Status;

import java.util.Date;

public class Task extends AbstractModel {

    private String name;

    private String description;

    private Status status = Status.NOT_STARTED;

    private Date dateFinish;

    private String TaskId;

    public Task() {
    }

    public Task(String name) {
        this.name = name;
    }

    public Task(String name, String TaskId) {
        this.name = name;
        this.TaskId = TaskId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public Date getDateFinish() {
        return dateFinish;
    }

    public void setDateFinish(Date dateFinish) {
        this.dateFinish = dateFinish;
    }

    public String getTaskId() {
        return TaskId;
    }

    public void setTaskId(String TaskId) {
        this.TaskId = TaskId;
    }


}
