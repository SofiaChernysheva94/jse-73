package ru.t1.chernysheva.tm.repository;

import org.springframework.stereotype.Repository;
import ru.t1.chernysheva.tm.model.Task;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

@Repository
public class TaskRepository {

    private static final TaskRepository INSTANCE = new TaskRepository();

    public static TaskRepository getInstance() {
        return INSTANCE;
    }

    private final Map<String, Task> tasks = new LinkedHashMap<>();

    {
        add(new Task("One"));
        add(new Task("Two"));
        add(new Task("Three"));
        add(new Task("Four"));
    }

    public void add(final Task Task) {
        tasks.put(Task.getId(), Task);
    }

    public void create() {
        add(new Task("New task " + System.currentTimeMillis()));
    }

    public void deleteById(final String id) {
        tasks.remove(id);
    }

    public Collection<Task> findAll() {
        return tasks.values();
    }

    public Task findById(final String id) {
        return tasks.get(id);
    }

    public void save(Task Task) {
        tasks.put(Task.getId(), Task);
    }

}
