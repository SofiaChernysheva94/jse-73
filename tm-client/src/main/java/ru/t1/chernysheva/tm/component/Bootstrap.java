package ru.t1.chernysheva.tm.component;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Component;
import ru.t1.chernysheva.tm.api.endpoint.*;
import ru.t1.chernysheva.tm.api.service.*;
import ru.t1.chernysheva.tm.event.ConsoleEvent;
import ru.t1.chernysheva.tm.exception.system.CommandNotSupportedException;
import ru.t1.chernysheva.tm.listener.AbstractListener;
import ru.t1.chernysheva.tm.exception.system.ArgumentNotSupportedException;
import ru.t1.chernysheva.tm.util.SystemUtil;
import ru.t1.chernysheva.tm.util.TerminalUtil;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;

@Component
public final class Bootstrap {

    @NotNull
    private final String commands = "ru.t1.chernysheva.tm.command";

    @NotNull
    @Autowired
    private ICommandService commandService;

    @NotNull
    @Autowired
    private IPropertyService propertyService;

    @NotNull
    @Autowired
    private ITokenService tokenService;

    @NotNull
    @Autowired
    private ILoggerService loggerService;

    @NotNull
    @Autowired
    private FileScanner fileScanner;

    @NotNull
    @Autowired
    private ISystemEndpoint systemEndpoint;

    @NotNull
    @Autowired
    private ITaskEndpoint taskEndpoint;

    @NotNull
    @Autowired
    private IProjectEndpoint projectEndpoint;

    @NotNull
    @Autowired
    private IUserEndpoint userEndpoint;

    @NotNull
    @Autowired
    private IAuthEndpoint authEndpoint;

    @NotNull
    @Autowired
    private AbstractListener[] listeners;

    @NotNull
    @Autowired
    private ApplicationEventPublisher publisher;

    private void processArguments(@Nullable String[] arguments) {
        if (arguments == null || arguments.length < 1) return;
        processArgument(arguments[0]);
        exit();
    }

    private static void exit() {
        System.exit(0);
    }

    private void processArgument(@Nullable final String argument) {
        @Nullable final AbstractListener listener = Arrays.stream(listeners)
                .filter(m -> argument.equals(m.getArgument()))
                .findFirst()
                .orElse(null);
        if (listener == null && argument != null) throw new ArgumentNotSupportedException(argument);
        if (listener == null) throw new ArgumentNotSupportedException();
        publisher.publishEvent(listener.getName());
    }

    private void prepareShutdown() {
        loggerService.info("** TASK MANAGER IS SHUTTING DOWN **");
        fileScanner.stop();
    }

    @SneakyThrows
    private void initPID() {
        @NotNull final String filename = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(filename), pid.getBytes());
        @NotNull final File file = new File(filename);
        file.deleteOnExit();
    }

    public void createConsoleEvent(@Nullable final String command) {
        @Nullable final AbstractListener listener = Arrays.stream(listeners)
                .filter(m -> command.equals(m.getName()))
                .findFirst()
                .orElse(null);
        if (listener == null) throw new CommandNotSupportedException(command);
        publisher.publishEvent(new ConsoleEvent(command));
    }

    private void initCommands() {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                System.out.println("ENTER COMMAND:");
                @NotNull final String command = TerminalUtil.nextLine();
                createConsoleEvent(command);
                System.out.println("[OK]");
                loggerService.command("Executing command: " + command);
            } catch (@NotNull final Exception e) {
                loggerService.error(e);
                System.out.println("[FAIL]");
            }
        }
    }

    private void prepareStartup() {
        initPID();
        loggerService.info("** WELCOME TO TASK MANAGER **");
        Runtime.getRuntime().addShutdownHook(new Thread(this::prepareShutdown));
        //fileScanner.start();
    }

    public void run(@Nullable String[] args) {
        processArguments(args);
        prepareStartup();
        initCommands();
    }

}
